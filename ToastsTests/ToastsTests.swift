//
//  ToastsTests.swift
//  ToastsTests
//
//  Created by Daniel Esteban Cardona Rojas on 9/21/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import XCTest
@testable import Toasts

class ToastsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSingleToastGetsRendered() {
        let expectation = XCTestExpectation(description: "User action called")
        let toast = Toast.custom(message: "Hi", title: nil, duration: 0.0)
        let mockDelegate = ToastManagerDelegateMock()
        let manager = ToastManager()
        manager.delegate = mockDelegate

        mockDelegate.renderHandler = { (toast: Toast) in
            expectation.fulfill()
        }

        manager.show(toast)

        wait(for: [expectation], timeout: 2.0)
    }
    
    func testFirstToastGetsRenderedBeforeSecond() {
        let toast1expectation = XCTestExpectation(description: "First toast rendered")
        let toast2expectation = XCTestExpectation(description: "Second toast rendered")
        let toast1 = Toast.custom(message: "First", title: nil, duration: 0.0)
        let toast2 = Toast.custom(message: "Second", title: nil, duration: 0.0)

        let mockDelegate = ToastManagerDelegateMock()
        let manager = ToastManager()
        manager.delegate = mockDelegate
        
        mockDelegate.renderHandler = { (toast: Toast) in
            toast1expectation.fulfill()
            
            if toast1expectation.expectedFulfillmentCount == 1 && toast.message == toast2.message {
                toast2expectation.fulfill()
            }
            
        }
        
        manager.show(toast1)
        manager.show(toast2)

        wait(for: [toast1expectation, toast2expectation], timeout: 2.0)
    }
    
    func testSecondToastIsProcessedAfterFirstDismissal() {
        let toast1expectation = XCTestExpectation(description: "First toast rendered")
        let toast2expectation = XCTestExpectation(description: "Second toast rendered")
        let toast1 = Toast.custom(message: "First", title: nil, duration: 0.0)
        let toast2 = Toast.custom(message: "Second", title: nil, duration: 0.0)
        let mockDelegate = ToastManagerDelegateMock()


        let manager = ToastManager()
        manager.delegate = mockDelegate
        
        mockDelegate.renderHandler = { toast in
            toast1expectation.fulfill()
            if toast1expectation.expectedFulfillmentCount == 1 && toast.message == toast2.message {
                toast2expectation.fulfill()
            }
        }
        

        manager.show(toast1)
        manager.show(toast2)

        wait(for: [toast1expectation, toast2expectation], timeout: 2.0)
    }
    
    func testPresentingNewToastForcessDismissOfPrevious() {
        // First toast is dismissed before completing its duration
        let toast1expectation = XCTestExpectation(description: "First toast is dismissed")
        let toast2expectation = XCTestExpectation(description: "Second toast is rendered after dismissing first")
        let toast1 = Toast.custom(message: "First", title: nil, duration: 1.0)
        let toast2 = Toast.custom(message: "Second", title: nil, duration: 1.0)
        let mockDelegate = ToastManagerDelegateMock()
        
        
        let manager = ToastManager()
        manager.delegate = mockDelegate
        
        mockDelegate.dismissHandler = { toast in
            // First toast is dimissed
            if toast.message == toast1.message {
                toast1expectation.fulfill()
            }
        }
        
        mockDelegate.renderHandler = { toast in
            // Second Toast is presented
            let isSecondToast = toast.message == toast2.message
            let firstToastHasBeenDismissed = toast1expectation.expectedFulfillmentCount == 1
            if isSecondToast && firstToastHasBeenDismissed {
                toast2expectation.fulfill()
            }
        }
        
        
        manager.show(toast1)
        manager.show(toast2)
        
        wait(for: [toast1expectation, toast2expectation], timeout: 4.0)
        
    }
    
    func testDelegateDimissIsOnlyRequestdTwiceForTwoToasts() {
        let expectation1 = XCTestExpectation(description: "Called twice")
        expectation1.expectedFulfillmentCount = 2
        expectation1.assertForOverFulfill = false
        let toast1 = Toast.custom(message: "First", title: nil, duration: 0.0)
        let toast2 = Toast.custom(message: "Second", title: nil, duration: 0.0)
        let mockDelegate = ToastManagerDelegateMock()
        let manager = ToastManager()
        
        manager.delegate = mockDelegate
        mockDelegate.dismissHandler = { toast in
            expectation1.fulfill()
        }
        
        manager.show(toast1)
        manager.show(toast2)
        
        wait(for: [expectation1], timeout: 3.0)
    }
    
    func testSecondToastIsAlwaysDismissedAfterFirst() {
        let expectation1 = XCTestExpectation(description: "First toast dismissed")
        let expectation2 = XCTestExpectation(description: "Second toast dismissed after first")
        let toast1 = Toast.custom(message: "First", title: nil, duration: 0.0)
        let toast2 = Toast.custom(message: "Second", title: nil, duration: 0.0)
        let mockDelegate = ToastManagerDelegateMock()
        let manager = ToastManager()
        
        manager.delegate = mockDelegate
        mockDelegate.dismissHandler = { toast in
            if toast.message == toast1.message {
                expectation1.fulfill()
            }
            
            let isSecondToast = toast.message == toast2.message
            let firstToastHasBeenDismissed = expectation1.expectedFulfillmentCount == 1
            if isSecondToast && firstToastHasBeenDismissed {
                expectation2.fulfill()
            }
        }
        
        manager.show(toast1)
        manager.show(toast2)
        
        wait(for: [expectation1, expectation2], timeout: 3.0)
        
    }
    
    func testPresentingNewToastForcessDismissOfPreviousWhenPushedInBackgroundThread() {
        let toast1expectation = XCTestExpectation(description: "First toast is dismissed")
        let toast2expectation = XCTestExpectation(description: "Second toast is rendered after dismissing first")
        let toast1 = Toast.custom(message: "First", title: nil, duration: 1.0)
        let toast2 = Toast.custom(message: "Second", title: nil, duration: 1.0)
        let mockDelegate = ToastManagerDelegateMock()
        let manager = ToastManager()
        
        manager.delegate = mockDelegate
        
        mockDelegate.dismissHandler = { toast in
            if toast.message == toast1.message {
                toast1expectation.fulfill()
            }
        }
        
        mockDelegate.renderHandler = { toast in
            let isSecondToast = toast.message == toast2.message
            let firstToastHasBeenDismissed = toast1expectation.expectedFulfillmentCount == 1
            if isSecondToast && firstToastHasBeenDismissed {
                toast2expectation.fulfill()
            }
        }
        
        
        manager.show(toast1)
        DispatchQueue.global(qos: .userInitiated).async {
            manager.show(toast2)
        }

        wait(for: [toast1expectation, toast2expectation], timeout: 4.0)
        
    }
    

}

// MARK: - TestManagerDelegate

public class ToastManagerDelegateMock: ToastManagerDelegate {
    // Ignore all view instantiation and presentation
    public typealias ToastHandler = (Toast) -> Void
    public var renderHandler: ToastHandler?
    public var presentHandler: ToastHandler?
    public var dismissHandler: ToastHandler?
    
    
    public init() {
    }
    
    public func dismiss(_ view: UIView, for toast: Toast) {
        dismissHandler?(toast)
    }
    
    public func render(toast: Toast) -> UIView {
        self.renderHandler?(toast)
        return UIView()
    }
    
    public func present(_ view: UIView, for toast: Toast) {
        self.presentHandler?(toast)
    }
    
    
}
