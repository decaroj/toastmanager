//
//  ViewController.swift
//  Toasts
//
//  Created by Daniel Esteban Cardona Rojas on 9/21/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let manager = ToastManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapToast1(_ sender: Any) {
        let toast = Toast.success(message: "Simple", title: nil)
        ToastManager.sharedInstance.show(toast)
    }
    
    @IBAction func didTapToast2(_ sender: Any) {
        let toast2 = Toast.interactive(message: "Hola", title: nil) {
           print($0)
        }
            
        ToastManager.sharedInstance.show(toast2)
    }
    
    @IBAction func didTapLoading(_ sender: Any) {
        let toast = Toast.loading(message: "Loading")
        ToastManager.sharedInstance.show(toast)
    }
    
}

