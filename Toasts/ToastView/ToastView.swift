//
//  ToastView.swift
//
//  Created by Daniel Esteban Cardona Rojas on 9/24/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

class ToastView: UIView {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    var toast: Toast?
    
    func commonInit() {
        Bundle.main.loadNibNamed("ToastView", owner: self, options: nil)
        addSubview(contentView)
        translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.heightAnchor.constraint(equalTo: heightAnchor),
            contentView.widthAnchor.constraint(equalTo: widthAnchor),
            contentView.centerXAnchor.constraint(equalTo: centerXAnchor),
            contentView.centerYAnchor.constraint(equalTo: centerYAnchor)
            ])
    }
    
    func configure(_ toast: Toast) {
        self.toast = toast
        actionButton.addTarget(self, action: #selector(userInteractionTapped(_:)), for: .touchUpInside)
        actionButton.setTitle(toast.buttonName, for: .normal)
        actionButton.isHidden = !toast.isInteractive
        loadingIndicator.isHidden = !Toast.equalKind(toast.kind, .loading)
        messageLabel.text = toast.message
    }
    
    @objc func userInteractionTapped(_ sender: UIButton) {
        guard let tst = self.toast else {
            return
        }
        
        tst.action?(tst)
        dismiss()
    }
    
    func present() {
        let transform = CGAffineTransform(translationX: 0, y: 80)
        self.transform = transform
        UIView.animate(withDuration: 0.3) {
            let transform = CGAffineTransform(translationX: 0, y: 0)
            self.transform = transform
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.5, animations: {
            let transform = CGAffineTransform(translationX: 0, y: 80)
            self.transform = transform
        }) { (toggle: Bool) in
            self.removeFromSuperview()
        }
    }
    
}
