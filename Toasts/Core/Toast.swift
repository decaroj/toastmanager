//
//  Toast.swift
//  Toasts
//
//  Created by Daniel Esteban Cardona Rojas on 9/21/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import Foundation
import UIKit

struct Toast {
    enum Kind {
        case success, failure, loading, interactive(buttonName: String)
    }
    typealias ToastHandler = (Toast) -> Void
    
    let kind: Kind
    let message: String
    let title: String?
    let action: ToastHandler?
    
    private init(kind: Kind, message: String, title: String?, action: ToastHandler?) {
        self.action = action
        self.title = title
        self.message = message
        self.kind = kind
    }
    
    // MARK: - Public API
    var duration: TimeInterval {
        var interval = 0.0
        switch kind {
        case .interactive(_):
            interval = 8.0
        default:
            interval = 6.0
        }
        
        return interval
    }
    
    var isInteractive: Bool {
        return Toast.equalKind(self.kind, .interactive(buttonName: ""))
    }
    
    var buttonName: String? {
        switch self.kind {
        case .interactive(let name):
            return name
        default:
            return nil
        }
    }
    
    static func equalKind(_ fst: Kind, _ snd: Kind) -> Bool {
        switch (fst, snd) {
        case (.interactive(_), Kind.interactive(_)):
            return true
        case (.success, .success):
            return true
        case (.failure, .failure):
            return true
        case (.loading, .loading):
            return true
        default:
            return false
        }
    }
    
    // Short hand initializers
    static func success(message: String, title: String?) -> Toast {
        return Toast(kind: .success, message: message, title: title, action: nil)
    }
    
    static func failure(message: String, title: String?) -> Toast {
        return Toast(kind: .failure, message: message, title: title, action: nil)
    }
    
    static func interactive(label: String = "OK", message: String, title: String?, action: @escaping ToastHandler) -> Toast {
        return Toast(kind: .interactive(buttonName: label), message: message, title: title, action: action)
    }
    
    static func custom(message: String, title: String?, duration: TimeInterval) -> Toast {
        return Toast(kind: .success, message: message, title: title, action: nil)
    }
    
    static func loading(message: String) -> Toast {
        return Toast(kind: .loading, message: message, title: nil, action: nil)
    }
    
    //App specific initializers
    
    
    var rendered: ToastView {
        let toastView = ToastView()
        toastView.configure(self)
        return toastView
    }
    
    
}
