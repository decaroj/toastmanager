//
//  ToastManager.swift
//  Toasts
//
//  Created by Daniel Esteban Cardona Rojas on 9/21/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import Foundation
import UIKit

// We need some type of lifecycle observer independently for each Toast

protocol ToastManagerDelegate: class {
    func render(toast: Toast) -> UIView
    func present(_ view: UIView, for toast: Toast)
    func dismiss(_ view: UIView, for toast: Toast)
}

class ToastManager {
    
    static let sharedInstance = ToastManager()
    /// Is a delay used to compensate for a toast being dismissed before completing its duration.
    static let compensationDelay = 0.8
    private let queue = DispatchQueue.main
    private let dispatchGroup = DispatchGroup()
    typealias ToastHandle = ((Toast) -> Void)
    
    var currentToast: Toast?
    var currentView: UIView?
    var currentPresentationTask: DispatchWorkItem?
    var currentDismissTask: DispatchWorkItem?
    
    weak var delegate: ToastManagerDelegate?
    
    func show(_ toast: Toast) {
        guard let delegatedObj = self.delegate else {
            return
        }
        
        // Cancel previous toast if there
        queue.async {
            let view = delegatedObj.render(toast: toast)
            if let item = self.currentDismissTask, let v = self.currentView, let tst = self.currentToast {
                item.cancel()
                delegatedObj.dismiss(v, for: tst)
                usleep(UInt32(ToastManager.compensationDelay * 1000000))
            }
            
            self.process(toast: toast, view: view, delegate: delegatedObj)
        }
        
    }
    
    private func process(toast: Toast, view: UIView, delegate delegatedObj: ToastManagerDelegate ) {
        let toastDuration = toast.duration
        let presentationTask = DispatchWorkItem {
            delegatedObj.present(view, for: toast)
        }
        
        let dismissTask = DispatchWorkItem {
            delegatedObj.dismiss(view, for: toast)
            self.currentToast = nil
        }
        
        // Schedule tasks
        queue.async(execute: presentationTask)
        queue.asyncAfter(deadline: .now() + toastDuration, execute: dismissTask)
        
        self.currentPresentationTask = presentationTask
        self.currentDismissTask = dismissTask
        self.currentView = view
        self.currentToast = toast
    }
    
    
}
